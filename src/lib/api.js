import axios from 'axios'

const key = 'AIzaSyAZsjBdSMJ7vseCLAUyEB89Mg8hjYJ7o7U'
const fetcher = axios.create({
    baseURL: 'https://youtube.googleapis.com/youtube/v3',
    params: {
        key: key
    }
})

export default fetcher;
