import React, { useState } from 'react';
import { Container } from 'react-bootstrap';
import MyNav from './components/MyNav';
import Video from "./components/Video";
import DetailedVideo from "./components/DetailedVideo";
import Channel from "./components/Channel";
import DetailedChannel from "./components/DetailedChannel";
import './App.css';

// router
import { BrowserRouter as Router, Switch, Route } from "react-router-dom";
import { If, Then, Else} from 'react-if';
const App = () => {
    const [videos, setVideos] = useState([]);
    const [selectedVideo, selectVideo] = useState({});
    const [selectedType, selectType] = useState("video");
    const [channels, setChannels] = useState([]);
    const [selectedChennel, selectChannel] = useState({});

    return (
        <Container className="p-3">
            <Router>

                <If condition={selectedType === "video"}>
                    <Then>
                    <MyNav onResults={setVideos} selectedType={selectedType} selectType={selectType} />
                    </Then>
                    <Else>
                    <MyNav onResults={setChannels} selectedType={selectedType} selectType={selectType} />
                    </Else>
                </If>
           
                <Switch>
                    <Route path="/videos/:videoId">
                        <DetailedVideo id={selectedVideo.id}
                            snippet={selectedVideo.snippet} player={selectedVideo.player}
                            statistics={selectedVideo.statistics} />
                    </Route>
                    <Route path="/channels/:channelId">
                        <DetailedChannel id={selectedChennel.id}
                            snippet={selectedChennel.snippet}
                            statistics={selectedChennel.statistics} />
                    </Route>
                    <Route path="/search/:type/:search">
                        <>
                        <If condition={selectedType === "video"}>
                            <Then>
                               { videos.map(v => {
                                    const {
                                        title, description, thumbnails,
                                        channelTitle, publishTime
                                    } = v.snippet;
    
                                    return (<Video
                                        videoId={v.id.videoId}
                                        thumbnail={thumbnails.high}
                                        description={description}
                                        channelTitle={channelTitle}
                                        publishTime={publishTime}
                                        title={title}
                                        selectVideo={selectVideo} />);
                                })
                            }
                            </Then>
                            <Else>  
                                { channels.map(c => {
                                    const {
                                         description, thumbnails,
                                        channelTitle, publishTime
                                    } = c.snippet;
    
                                    return (<Channel
                                        channelId={c.id.channelId}
                                        thumbnail={thumbnails.high}
                                        description={description}
                                        channelTitle={channelTitle}
                                        publishTime={publishTime}
                                        selectChannel={selectChannel} />);
                                })
                            }
                            </Else>
                        </If>   
                        </>
                    </Route>
                    {/* <Route path="/search/channels/:search">
                        <>
                            {channels.map(v => {
                                const {
                                    description,
                                    channelTitle
                                } = v.snippet;

                                return (<Channel
                                    videoId={v.id.videoId}
                                    description={description}
                                    channelTitle={channelTitle}
                                    selectChannel={selectChannel} />);
                            })}
                        </>
                    </Route> */}
                    <Route path="/">
                        Merci d'effectuer une recherche...
                    </Route>
                </Switch>
            </Router>
        </Container>
    );
};

export default App;
