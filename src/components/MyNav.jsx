import Navbar from "react-bootstrap/Navbar";
import {Nav} from "react-bootstrap";
import SearchBar from './SearchBar';

const MyNav = ({onResults, selectedType, selectType}) => {
    return (<Navbar bg="dark" variant="dark">
        <Navbar.Brand href="#home">Reactube</Navbar.Brand>
        <Nav className="mr-auto">
            <Nav.Link onClick={() => selectType("video")} >Videos</Nav.Link>
            <Nav.Link onClick={() => selectType("channel")} >Channels</Nav.Link>
        </Nav>
        <SearchBar onResults={onResults} selectedType = {selectedType} />
    </Navbar>);
}

export default MyNav;